import PIL.Image
import PIL.ImageTk
from tkinter import *
from ivy.ivy import IvyServer
import time
import os
import sys
from model.card import card
from model.player import player

class Client(IvyServer):

    def __init__(self, app, name, ip, port):
        IvyServer.__init__(self, name)
        self.app = app
        self.name = name
        self.start(ip+':'+port)
        self.bind_direct_msg(self.handle_direct_data)
        self.bind_msg(self.handle_withBackCard, 'V_backCardON:(.*)')
        self.bind_msg(self.handle_status_update, 'V_gameStatus:(.*)')
        self.bind_msg(self.handle_playersInfo_update, 'V_playersInfo:(.*)')
        self.bind_msg(self.handle_cards, 'V_cards:(.*)')
        self.bind_msg(self.handle_battlewinner, 'V_battleWinner:(.*)')
        self.bind_msg(self.handle_disconnect, 'V_disconnect')
        self.bind_msg(self.handle_gamewinner, 'V_gameWinner:(.*)')

    def handle_disconnect(self, agent):
        self.app.on_closing()

    def handle_withBackCard(self, agent, data):
        if data == 'True':
            self.app.backCardON = True
        else:
            self.app.backCardON = False

    def handle_gamewinner(self, agent, data):



        self.app.waitingPutCard = False
        self.app.waitingTakePot = False

        self.app.master.board.delete("all")

        if data == self.app.username:
            print("VICTOIRE")
            self.app.infoText = "Vous remportez la PARTIE!!!!!!!!!!!"
            im = PIL.Image.open(os.path.join(os.path.dirname(__file__), 'img/win.png'))
            self.app.master.board.imageEND = PIL.ImageTk.PhotoImage(im)
            end = self.app.master.board.create_image(0, 0, image=self.app.master.board.imageEND, anchor='nw')

        else:
            print("DEFAITE")
            self.app.infoText = "Vous avez perdu la PARTIE!!!!!!!!!!"
            im = PIL.Image.open(os.path.join(os.path.dirname(__file__), 'img/lost.png'))
            self.app.master.board.imageEND = PIL.ImageTk.PhotoImage(im)
            end = self.app.master.board.create_image(0, 0, image=self.app.master.board.imageEND, anchor='nw')

        self.app.updateGameStatus()

    def handle_battlewinner(self, agent, data):
        # p1:toto
        index,name = data.split(':')

        if name == self.app.username:
            self.app.infoText = "Vous remportez cette bataille! Prenez vos gains"
            self.app.waitingTakePot = True
            self.app.master.board.tag_raise(self.app.master.arrow2)
            self.app.master.board.tag_raise(self.app.master.winner)
            self.app.master.board.coords(self.app.master.winner, 892.5, 500)
        else:
            self.app.infoText = "Le joueur " + name + " remporte cette bataille!"
        self.app.updateGameStatus()

    def handle_cards(self, agent, data):
        # p1:red,As,Pique,14;black,Dame,Coeur,11/p2:red,As,Pique,14
        # p2:red,As,Pique,14;black,Dame,Coeur,11/p1:red,As,Pique,14

        rdata = str.split(data.rstrip('/'),'/',4) # DECOUPAGE PAR JOUEUR


        for i in range(0,4):
            self.app.players[i].deck.clear()

        if rdata != ['']:

            if len(rdata) > 0:
                data1 = str.split(rdata[0][3:], ';')
                posData1 = int(rdata[0][1:2])-1
                for i in range(0, len(data1)-1):
                    cardData1 = data1[i].split(',', 4)
                    self.app.players[posData1].deck.append(card(cardData1[0],cardData1[1],cardData1[2],cardData1[3]))
                self.app.players[posData1].nbcards = data1[-1]
                print(data1[-1])
            if len(rdata) > 1:
                data2 = str.split(rdata[1][3:], ';')
                posData2 = int(rdata[1][1:2])-1
                for i in range(0, len(data2)-1):
                    cardData2 = data2[i].split(',', 4)
                    self.app.players[posData2].deck.append(card(cardData2[0],cardData2[1],cardData2[2],cardData2[3]))
                self.app.players[posData2].nbcards = data2[-1]
            if len(rdata) > 2:
                data3 = str.split(rdata[2][3:], ';')
                posData3 = int(rdata[2][1:2])-1
                for i in range(0, len(data3)-1):
                    cardData3 = data3[i].split(',', 4)
                    self.app.players[posData3].deck.append(card(cardData3[0],cardData3[1],cardData3[2],cardData3[3]))
                self.app.players[posData3].nbcards = data3[-1]
            if len(rdata) > 3:
                data4 = str.split(rdata[3][3:], ';')
                posData4 = int(rdata[3][1:2])-1
                for i in range(0, len(data4)-1):
                    cardData4 = data4[i].split(',', 4)
                    self.app.players[posData4].deck.append(card(cardData4[0], cardData4[1], cardData4[2], cardData4[3]))
                self.app.players[posData4].nbcards = data4[-1]
        else:
            if int(self.app.players[self.app.getPlayerByName(self.app.username)].nbcards) > 0:
                self.app.waitingPutCard = True
            self.app.emptyBoard()
            self.app.infoText=""
        self.app.writePlayersInfo()

        if self.app.connected:
            self.app.drawCards()
            self.app.updateGameStatus()



    def handle_playersInfo_update(self, agent, data):
        #V_playersInfo:Toto/12;John/10
        p1info,p2info,p3info,p4info = str.split(data, ';', 4)

        p1username,p1nbcards = str.split(p1info, '/', 2)
        p2username, p2nbcards = str.split(p2info, '/', 2)
        p3username, p3nbcards = str.split(p3info, '/', 2)
        p4username, p4nbcards = str.split(p4info, '/', 2)

        self.app.players[0].name = p1username
        self.app.players[1].name = p2username
        self.app.players[2].name = p3username
        self.app.players[3].name = p4username

        self.app.players[0].nbcards = int(p1nbcards)
        self.app.players[1].nbcards = int(p2nbcards)
        self.app.players[2].nbcards = int(p3nbcards)
        self.app.players[3].nbcards = int(p4nbcards)

        if self.app.connected:
            self.app.writePlayersInfo()



    def handle_status_update(self, agent, data):
        self.app.statusText = data
        if data.startswith( 'PARTIE EN COURS' ):
            self.app.playing = True
            self.app.waitingPutCard = True

        if self.app.connected:
            self.app.updateGameStatus()

    def handle_direct_data(self, client, num_id, data):
        print(data)
        msg,text = str.split(data, ';')
        if msg=="P_connect:success":
            self.app.connected = True
            self.app.errortext = text
        elif msg=="P_connect:fail":
            self.app.errortext = text
        else:
            print(msg)

class App:
    def __init__(self, master):
        self.master = master
        self.connected = False
        self.playing = False
        self.waitingPutCard = False
        self.waitingTakePot = False
        self.errortext = ""
        self.username = "Unnamed";
        self.statusText = ""
        self.infoText = ""
        self.players = []
        self.nbPlayersMax = 2
        self.backCardON = True
        self.home()


    def home(self):

        ## INIT PLAYERS
        self.players.append(player(""))
        self.players.append(player(""))
        self.players.append(player(""))
        self.players.append(player(""))

        self.master.frame = LabelFrame(self.master, text="Cards War - Connexion", borderwidth=2, relief=GROOVE)
        self.master.frame.pack(padx=30, pady=30)

        # Titre de la fenetre
        self.master.title("CardsWar")
        # Icone de la fenetre
        self.master.wm_iconbitmap(bitmap="cards.ico")

        pseudo = StringVar(self.master, "Toto")
        titleLabel = Label(self.master.frame, text="Pseudo :")
        titleLabel.pack(padx=5, pady=5)
        self.master.pseudo = Entry(self.master.frame, textvariable=pseudo, width=30)
        self.master.pseudo.pack(padx=5, pady=5)

        ip_address = StringVar(self.master, "127.0.0.1:2010")
        titleLabel = Label(self.master.frame, text="Adresse ip :")
        titleLabel.pack(padx=5, pady=5)
        self.master.input_ip = Entry(self.master.frame, textvariable=ip_address, width=30)
        self.master.input_ip.pack(padx=5, pady=5)

        self.master.button_connect = Button(self.master.frame, text='Connect', width=25, cursor="target",
                                     command=lambda: self.connect(pseudo.get(), ip_address.get()))
        self.master.button_connect.pack(padx=5, pady=5)

        self.master.errorLabel = Label(self.master.frame, fg="red", text="")
        self.master.errorLabel.pack(padx=5, pady=5)

        ## Canvas de jeu
        self.master.board = Canvas(self.master.frame, width=992, height=600, highlightthickness=0, relief='ridge')


    def connect(self, pseudo, ip_address):

        self.errortext = ""
        ip,port = ip_address.split(':')
        self.client = Client(self, pseudo, ip, port)
        time.sleep(0.3)
        self.send("G_connect:"+pseudo)
        time.sleep(0.3)

        if (self.connected):
            self.username = pseudo;
            self.showGame()
        else:
            self.client.stop()
            if self.errortext == "":
                self.errortext = "Host not responding... Try again!"
            self.master.errorLabel["text"] = self.errortext



    def showGame(self):

        root.configure(background='#942333')

        # Destruction du formulaire de connexion
        for w in self.master.frame.winfo_children():
            w.pack_forget()

        # Titre de la fenetre
        self.master.title("CardsWar - Bataille")
        # Icone de la fenetre
        self.master.wm_iconbitmap(bitmap="cards.ico")

        # Frame
        self.master.frame.pack_forget()
        self.master.frame = Frame(self.master)
        self.master.frame["width"] = 992
        self.master.frame["height"] = 600
        self.master.frame["bg"] = "#942333"
        self.master.frame.pack(expand=YES, fill=BOTH)

        self.master.board = Canvas(self.master.frame, width=992, height=600, highlightthickness=0, relief='ridge')
        self.master.board.pack(expand=YES, fill=BOTH)



        ####
        # Background
        ####
        # Load the image file
        im = PIL.Image.open(os.path.join(os.path.dirname(__file__), 'img/bg.jpg'))
        # Put the image into a canvas compatible class, and stick in an
        # arbitrary variable to the garbage collector doesn't destroy it
        self.master.board.image = PIL.ImageTk.PhotoImage(im)
        # Add the image to the canvas, and set the anchor to the top left / north west corner
        self.master.board.create_image(0, 0, image=self.master.board.image, anchor='nw')


        self.master.usernameText = Label(self.master.board, fg="white", bg='#942333', text="Bienvenue "+self.username)
        self.master.usernameText.pack()
        self.master.board.create_window(480, 15, window=self.master.usernameText)

        self.master.gameInfo = Label(self.master.board, fg="white", bg='#942333', text="Info: ")
        self.master.gameInfo.pack()
        self.master.board.create_window(480, 35, window=self.master.gameInfo)

        self.master.gameInfo2 = Label(self.master.board, fg="white", bg='#942333', text="Info: ")
        self.master.gameInfo2.pack()
        self.master.board.create_window(480, 55, window=self.master.gameInfo2)

        self.master.boardArea = self.master.board.create_rectangle(55, 183, 736, 526, tags="boardArea", fill="white")
        self.master.board.tag_lower(self.master.boardArea)
        self.master.boardAreaBorder = self.master.board.create_rectangle(55, 183, 736, 526, tags="boardArea", outline="#DE8F2A")
        self.master.board.tag_lower(self.master.boardAreaBorder)

        self.master.board.CardsImages = {'p1': [], 'p2': [], 'p3': [], 'p4': []}

        imdeck = PIL.Image.open(os.path.join(os.path.dirname(__file__), 'img/deck.png'))
        imdeck = imdeck.resize((150, 192), PIL.Image.ANTIALIAS)
        self.master.board.DeckImage = PIL.ImageTk.PhotoImage(imdeck)
        self.master.board.create_image(900, 300, image=self.master.board.DeckImage)

        imarrow = PIL.Image.open(os.path.join(os.path.dirname(__file__), 'img/arrow.png'))
        imarrow = imarrow.resize((70, 70), PIL.Image.ANTIALIAS)
        self.master.board.ArrowImage = PIL.ImageTk.PhotoImage(imarrow)
        self.master.arrow = self.master.board.create_image(720, 311, image=self.master.board.ArrowImage, tags="arrow")
        self.master.board.tag_lower(self.master.arrow)

        imback = PIL.Image.open(os.path.join(os.path.dirname(__file__), 'img/back.png'))
        imback = imback.resize((87, 122), PIL.Image.ANTIALIAS)
        self.master.board.BackImage = PIL.ImageTk.PhotoImage(imback)
        self.master.dcard = self.master.board.create_image(896, 311, image=self.master.board.BackImage, tags="dcard")

        imwinner = PIL.Image.open(os.path.join(os.path.dirname(__file__), 'img/winner.png'))
        imwinner = imwinner.resize((174, 174), PIL.Image.ANTIALIAS)
        self.master.board.WinnerButton = PIL.ImageTk.PhotoImage(imwinner)
        self.master.winner = self.master.board.create_image(892.5, 500, image=self.master.board.WinnerButton, tags="winner")
        self.master.board.tag_lower(self.master.winner)

        imarrow2 = PIL.Image.open(os.path.join(os.path.dirname(__file__), 'img/arrow.png'))
        imarrow2 = imarrow2.resize((70, 70), PIL.Image.ANTIALIAS)
        self.master.board.ArrowImage2 = PIL.ImageTk.PhotoImage(imarrow2)
        self.master.arrow2 = self.master.board.create_image(720, 510, image=self.master.board.ArrowImage2, tags="arrow2")
        self.master.board.tag_lower(self.master.arrow2)

        self.master.board.bind('<ButtonPress-1>', self.onObjectClickEvent)
        self.master.board.bind('<ButtonRelease-1>', self.endDrag)
        self.master.board.bind('<B1-Motion>', self.dragObject)
        self.currentObject = None

        ####LABELS JOUEURS
        p1info = self.players[0].name
        p2info = self.players[1].name
        p3info = self.players[2].name
        p4info = self.players[3].name



        if self.players[0].name != "":
            p1info += ' (' + str(self.players[0].nbcards) + ')'
        if self.players[1].name != "":
            p2info += ' (' + str(self.players[1].nbcards) + ')'
        if self.players[2].name != "":
            p3info += ' (' + str(self.players[2].nbcards) + ')'
        if self.players[3].name != "":
            p4info += ' (' + str(self.players[3].nbcards) + ')'
        self.master.p1 = Label(self.master.board, fg="white", bg='#7b1e2b', text=p1info)
        self.master.p2 = Label(self.master.board, fg="white", bg='#7b1e2b', text=p2info)
        self.master.p3 = Label(self.master.board, fg="white", bg='#7b1e2b', text=p3info)
        self.master.p4 = Label(self.master.board, fg="white", bg='#7b1e2b', text=p4info)
        self.master.p1.pack();
        self.master.p2.pack();
        self.master.p3.pack();
        self.master.p4.pack();
        self.master.board.create_window(200, 170, window=self.master.p1)
        self.master.board.create_window(332, 170, window=self.master.p2)
        self.master.board.create_window(464, 170, window=self.master.p3)
        self.master.board.create_window(596, 170, window=self.master.p4)
        ### FIN LABELS JOUEURS



        self.updateGameStatus()

    def emptyBoard(self):

        self.master.board.CardsImages = {'p1': [], 'p2': [], 'p3': [], 'p4': []}

    def drawCards(self):

        i = 1
        for c in self.players[0].deck:
            if self.backCardON and i % 2 == 0:
                self.showCardBack(1, i)
            else:
                self.showCard(1, i, c)
            i += 1

        i = 1
        for c in self.players[1].deck:
            if self.backCardON and i % 2 == 0:
                self.showCardBack(2, i)
            else:
                self.showCard(2, i, c)
            i += 1

        i = 1
        for c in self.players[2].deck:
            if self.backCardON and i % 2 == 0:
                self.showCardBack(3, i)
            else:
                self.showCard(3, i, c)
            i += 1

        i = 1
        for c in self.players[3].deck:
            if self.backCardON and i % 2 == 0:
                self.showCardBack(4, i)
            else:
                self.showCard(4, i, c)
            i += 1

    ##DRAG

    def startDrag(self,event): #on recupere la position de depart de l objet
        #self.moveToTopLevel(event) #mise au premier plan


        obj = self.master.board.find_withtag(CURRENT)
        if (self.waitingPutCard and len (obj) == 1 and "dcard" in self.master.board.gettags(obj[0])): #on test si l objet existe bien
            self.currentObject = obj[0]
            self.originalDcardCoords = self.master.board.coords(self.currentObject)
            self.mouse = (event.x, event.y) #stockage position actuelle


            self.master.board.tag_lower(self.master.arrow)



        else:
            self.currentObject = None

    def endDrag(self, event):

        if self.waitingPutCard and self.currentObject != None:
            if self.master.boardArea in self.master.board.find_overlapping(*self.master.board.bbox(self.currentObject)):
                    self.client.send_msg("G_putCard:"+self.username)
                    print("Je pose ma carte")
                    self.waitingPutCard = False
                    self.master.board.tag_lower(self.master.arrow)
            else:
                self.master.board.tag_raise(self.master.arrow)
            self.master.board.tag_lower(self.master.boardAreaBorder)
            self.master.board.coords(self.currentObject, self.originalDcardCoords[0], self.originalDcardCoords[1])



    def dragObject(self,event): #deplacement de l objet
        if not self.waitingPutCard or self.currentObject == None or self.master.board.gettags(self.currentObject)[0] != "dcard":
            return

        if self.master.boardArea in self.master.board.find_overlapping(*self.master.board.bbox(self.currentObject)):
            self.master.board.tag_raise(self.master.boardAreaBorder)
        else:
            self.master.board.tag_lower(self.master.boardAreaBorder)

        coords = self.master.board.coords(self.currentObject)
        dx = event.x - self.mouse [0] #variations de la position de la souris
        dy = event.y - self.mouse [1]
        self.master.board.coords(self.currentObject,coords[0]+dx,coords[1]+dy) #on applique les nouvelles positions
        self.mouse = (event.x, event.y) #stockage de la position du pointeur

    def clickWinner(self, event):

        obj = self.master.board.find_withtag(CURRENT)
        if (self.waitingTakePot and len(obj) == 1 and "winner" in self.master.board.gettags(obj[0])):  # on test si l objet existe bien
            self.currentObject = obj[0]

            self.master.board.tag_lower(self.master.arrow2)
            self.master.board.tag_lower(self.master.winner)

            self.waitingTakePot = False

            self.client.send_msg("G_potTaken:"+self.username)



        else:
            self.currentObject = None

    def onObjectClickEvent(self, event):
        obj = self.master.board.find_withtag(CURRENT)
        if ("dcard" in self.master.board.gettags(obj[0])):
            self.startDrag(event)
        elif ("winner" in self.master.board.gettags(obj[0])):
            self.clickWinner(event)


    def writePlayersInfo(self):
        p1info = self.players[0].name
        p2info = self.players[1].name
        p3info = self.players[2].name
        p4info = self.players[3].name

        if self.players[0].name != "":
            p1info += ' ('+str(self.players[0].nbcards)+')'
        if self.players[1].name != "":
            p2info += ' ('+str(self.players[1].nbcards)+')'
        if self.players[2].name != "":
            p3info += ' ('+str(self.players[2].nbcards)+')'
        if self.players[3].name != "":
            p4info += ' ('+str(self.players[3].nbcards)+')'

        self.master.p1["text"] = p1info
        self.master.p2["text"] = p2info
        self.master.p3["text"] = p3info
        self.master.p4["text"] = p4info




    def showCardBack(self, playerPos, cardPos):
        posX = 200 + ((playerPos-1)*132)
        posY = 275 + ((cardPos-1)*60)

        ## STORING IMAGE
        player = 'p' + str(playerPos)

        im = PIL.Image.open(os.path.join(os.path.dirname(__file__), 'img/back.png'))
        im = im.resize((85, 125), PIL.Image.ANTIALIAS)

        img = PIL.ImageTk.PhotoImage(im)

        self.master.board.CardsImages.get(player).append(img)

        ## SHOWING IMAGE
        self.master.board.create_image(posX, posY, image=self.master.board.CardsImages.get(player)[-1])

    def showCard(self, playerPos, cardPos, card):

        ## CALCULATE COORDS
        posX = 200 + ((playerPos-1)*132)
        posY = 275 + ((cardPos-1)*60)

        player = 'p'+str(playerPos)

        ## STORING IMAGE
        self.master.board.CardsImages.get(player).append(self.getCardImage(card))

        ## SHOWING IMAGE
        self.master.board.create_image(posX, posY, image=self.master.board.CardsImages.get(player)[-1])


    def getCardImage(self, c:card):
        path = "img/"

        #Color + Symbol
        if c.color == "red":
            path += "red/"
            if c.symbol == "Coeur":
                path += "coeur/"
            else:
                path += "carreau/"
        else:
            path += "black/"
            if c.symbol == "Trèfle":
                path += "trefle/"
            else:
                path += "pique/"

        path += str.lower(c.rank)

        path += '.png'

        im = PIL.Image.open(os.path.join(os.path.dirname(__file__), path))
        im = im.resize((85, 125),PIL.Image.ANTIALIAS)

        return PIL.ImageTk.PhotoImage(im)

    def getPlayerByName(self, name):
        if self.players[0].name == name:
            return 0
        elif self.players[1].name == name:
            return 1
        elif self.players[2].name == name:
            return 2
        elif self.players[3].name == name:
            return 3

    def send(self, msg):
        self.client.send_msg(msg)
        print("MSG SENT : "+msg)

    def updateGameStatus(self):
        self.master.gameInfo["text"] = self.statusText
        self.master.gameInfo2["text"] = self.infoText
        if self.waitingPutCard:
            self.master.board.tag_raise(self.master.arrow)
        else:
            self.master.board.tag_lower(self.master.arrow)

    def on_closing(self):
        if self.connected:
            self.send("G_disconnect:"+self.username)
        self.master.destroy()
        sys.exit(1)

if __name__ == "__main__":
    root = Tk()
    root.wm_attributes("-topmost", 1)

    app = App(root)

    root.protocol("WM_DELETE_WINDOW", app.on_closing)
    root.mainloop()




