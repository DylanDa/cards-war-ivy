from model.basic_deck import basic_deck
from model.french54_deck import french54_deck
from model.deck_test import deck_test
from model.player import player
from ivy.ivy import IvyServer
from tkinter import *
from datetime import datetime
import time
import sys

class Server(IvyServer):
    def __init__(self, app, name, ip, port, nbj):
        IvyServer.__init__(self, name)
        self.app = app
        self.unique = True
        self.name = name
        self.start(ip + ':' + port)
        self.bind_msg(self.handle_connect, 'G_connect:(.*)')
        self.bind_msg(self.handle_disconnect, 'G_disconnect:(.*)')
        self.bind_msg(self.handle_unique, 'G_isUnique')
        self.bind_msg(self.handle_putCard, 'G_putCard:(.*)')
        self.bind_msg(self.handle_potTaken, 'G_potTaken:(.*)')
        self.bind_direct_msg(self.handle_direct_msg)

    def handle_potTaken(self, agent, data):
        #G_potTaken:Toto
        self.app.writeConsole("Le joueur "+data+" a récupéré ses gains!")
        self.app.clearPot()

        plist = [item for item in self.app.takenSlot if item not in self.app.losers]

        if len(plist) == 1:
            self.app.writeConsole("Le joueur " + self.app.players_list[plist[0]].name+ " remporte la PARTIE!!!!!")
            self.send_msg("V_gameWinner:"+self.app.players_list[plist[0]].name)


    def handle_putCard(self, agent, data):

        if self.app.isStarted:

            self.app.putCard(data)
            self.app.updateViews()
            self.app.writeConsole("Le joueur " + data + " a posé sa carte!")

            if self.app.takenSlot == self.app.putCardPlayers:
                self.app.writeConsole("Toutes les cartes sont sur le tapis!")
                self.app.battle()



    def handle_direct_msg(self, server, num_id, data):
        if (data != self.name):
            self.unique = False

    def handle_unique(self, agent):
        self.send_direct_message(agent, 0, 'G_notUnique:'+self.name, stop_on_first=False)


    def handle_connect(self, agent, msg):

        if (self.app.connected_players_nb < self.app.nbj) and self.app.isAvailable(msg) and not self.app.playing:
        ### CONNEXION REUSSIE
            # Ajout d'un joueur!
            self.app.addPlayer(msg)
            # Accusé de connection vers le nouveau joueur
            self.send_direct_message(msg, 0, "P_connect:success;Connection successful")
            self.send_msg("V_backCardON:"+str(self.app.backCardOn))


        ### ERREUR DE CONNEXION

            # SERVEUR PLEIN
        elif self.app.connected_players_nb == self.app.nbj:
            self.app.writeConsole("%s a tenté de se connecter mais le nombre de joueurs maximum est atteint." % msg)
            self.send_direct_message(msg, 0, "P_connect:fail;Server is full!")
            # PSEUDO DEJA UTILISE
        elif not self.app.isAvailable(msg):
            self.app.writeConsole("Un nouveau joueur %s a tenté de se connecter mais ce pseudo est déjà utilisé." % msg)
            self.send_direct_message(msg, 0, "P_connect:fail;Username already taken!", stop_on_first=False)
            # SERVEUR PLEIN + DEJA EN JEU
        elif self.app.playing:
            self.app.writeConsole("Un nouveau joueur %s a tenté de se connecter mais la partie à déjà démarré." % msg)
            self.send_direct_message(msg, 0, "P_connect:fail;Match already started!", stop_on_first=False)

    def handle_disconnect(self, agent, msg):
        self.app.removePlayer(msg)


class App:

    ############################
    # Interface Graphique
    ############################

    def __init__(self, master):
        self.name = ""
        self.master = master
        self.playing = False
        self.isStarted = False
        self.connected_players_nb = 0
        self.players_list = {'p1' : player, 'p2' : player, 'p3' : player, 'p4' : player}
        self.players_list['p1'] = player("")
        self.players_list['p2'] = player("")
        self.players_list['p3'] = player("")
        self.players_list['p4'] = player("")
        self.availableSlot = ['p1', 'p2', 'p3', 'p4']
        self.takenSlot = []
        self.pot = {'p1' : basic_deck, 'p2' : basic_deck, 'p3' : basic_deck, 'p4' : basic_deck}
        self.pot['p1'] = basic_deck("Pot P1")
        self.pot['p2'] = basic_deck("Pot P2")
        self.pot['p3'] = basic_deck("Pot P3")
        self.pot['p4'] = basic_deck("Pot P4")
        self.putCardPlayers = []
        self.battlefield = []
        self.losers = []
        self.home()

        # PERIODIC CALL
        #self.periodicCall()

    def periodicCall(self):
        """
        Check every 500 ms
        """
        #self.master.after(500, self.periodicCall)


    # PAGE DE CONFIGURATION
    def home(self):

        # Main Frame
        self.master.frame = LabelFrame(self.master, text="Cards War - Serveur", borderwidth=2, relief=GROOVE)
        self.master.frame.pack(padx=30, pady=30)

        # Titre de la fenetre
        self.master.title("CardsWar")
        # Icone de la fenetre
        self.master.wm_iconbitmap(bitmap="cards.ico")

        # Liste des modes de jeu
        self.master.select_mode = Listbox(self.master.frame, width=30, height=2, cursor="target", selectmode="single")
        self.master.select_mode.insert(END, "Classique")
        self.master.select_mode.insert(END, "Courte")
        self.master.select_mode.pack(padx=5, pady=5)
        self.master.select_mode.select_set(0)

        # Avec ou sans carte à l'envers
        self.backCardOn = BooleanVar()

        self.master.backcard = Checkbutton(self.master.frame, text="Avec carte à l'envers", variable=self.backCardOn)
        self.master.backcard.toggle()
        self.master.backcard.pack(padx=5, pady=5)

        # Choix nb joueurs
        self.master.select_nbj = Spinbox(self.master.frame, from_=2, to=4)
        self.master.select_nbj.pack(padx=5, pady=5)

        # Adresse ip & port du serveur
        ip_address = StringVar(self.master.frame, "127.0.0.1:2010")
        titleLabel = Label(self.master.frame, text="Adresse ip :")
        titleLabel.pack(padx=5, pady=5)
        self.master.input_ip = Entry(self.master.frame, textvariable=ip_address, width=30)
        self.master.input_ip.pack(padx=5, pady=5)

        # Lancer le serveur
        self.master.button_start = Button(self.master.frame, text='Start', width=25, cursor="target",
                                     command=lambda: self.start(str(self.master.select_mode.curselection()), ip_address.get(), self.master.select_nbj.get()))
        self.master.button_start.pack(padx=5, pady=5)

        self.master.errorLabel = Label(self.master.frame, fg="red", text="")
        self.master.errorLabel.pack(padx=5, pady=5)

    # TENTATIVE DE DEMARRAGE
    def start(self, mode, ip_address, nbj):
        self.nbj = int(nbj)
        self.ip_adress = ip_address
        self.mode = mode

        ip,port = ip_address.split(':')
        self.server = Server(self, "Jeu", ip, port, nbj)
        self.name = self.server.name
        self.server.send_msg("G_isUnique")
        time.sleep(0.2)
        if (self.server.unique):
            self.started()
            if self.backCardOn.get() == True:
                self.backCardOn = True
            else:
                self.backCardOn = False

        else:
            self.master.errorLabel["text"] = "Serveur existant sur cette adresse:port!"


    # SERVEUR EN LIGNE - Changement de la vue
    def started(self):



        # Destruction du formulaire
        for w in self.master.frame.winfo_children():
            w.pack_forget()


        # Titre de la fenetre
        self.master.title("CardsWar - Bataille Serveur")
        # Icone de la fenetre
        self.master.wm_iconbitmap(bitmap="cards.ico")

        # Frame Info
        self.master.topFrame = LabelFrame(self.master, text="Informations", borderwidth=2, relief=GROOVE)
        self.master.topFrame.pack(side=TOP, padx=5, pady=5, fill=X)

        # Labels info

        self.master.labelGameStatus = Label(self.master.topFrame, text="ETAT DU SERVEUR: \n\nETAT DU JEU: \n\nJoueur(s) connecté(s) : ")
        self.master.labelGameStatus.pack(anchor=W, side=LEFT)

        self.master.labelServerStatusValue = Label(self.master.topFrame, text="EN LIGNE", fg="green")
        self.master.labelServerStatusValue.pack()
        self.master.labelGameStatusValue = Label(self.master.topFrame, text="EN ATTENTE DE JOUEURS", fg="red")
        self.master.labelGameStatusValue.pack(pady=7)
        self.master.labelNbp = Label(self.master.topFrame, fg="red", text=str(self.connected_players_nb)+" sur "+str(self.nbj))
        self.master.labelNbp.pack()

        self.master.labelPlayersList = Label(self.master, fg="black", text=str(self.playersListToStr()))
        self.master.labelPlayersList.pack()

        # Frame Console
        self.master.frame["text"] = "Console"
        self.master.frame.pack(side=BOTTOM, padx=5, pady=5, fill=X)

        # Console / Terminal
        self.master.console = Text(self.master.frame)
        self.master.console.pack(padx=5, pady=5)
        self.writeConsole("Serveur en ligne! En attente de %s joueurs. " % (self.nbj-self.connected_players_nb))

        self.isStarted = True

    def putCard(self, pname):
        key = self.getPlayerPos(pname)
        self.putCardPlayers.append(key)
        self.putCardPlayers.sort()

        self.pot[key].append(self.players_list[key].deck.first().clone())
        self.players_list[key].deck.remove_first()
        self.players_list[key].nbcards = len(self.players_list[key].deck)



    def clearPot(self):
        self.putCardPlayers = []

        self.pot['p1'] = basic_deck("Pot joueur 1")
        self.pot['p2'] = basic_deck("Pot joueur 2")
        self.pot['p3'] = basic_deck("Pot joueur 3")
        self.pot['p4'] = basic_deck("Pot joueur 4")

        self.updateViews()


    def updateViews(self):

        dataToSend = "V_cards:"
        for key in self.putCardPlayers:
            dataToSend += key + ":"
            for c in self.pot[key]:
                dataToSend += c.color + ','
                dataToSend += c.rank + ','
                dataToSend += c.symbol + ','
                dataToSend += str(c.value) + ';'
            dataToSend += str(self.players_list[key].nbcards)
            dataToSend += '/'


        self.server.send_msg(dataToSend)



    def getPlayerPos(self, name):
        for key,item in self.players_list.items():
            if item.name == name:
                return key


    def playersListToStr(self):

        res = "Liste des joueurs : "
        res += self.players_list['p1'].name
        res += "  -  "+self.players_list['p2'].name
        if self.nbj == 3:
            res += "  -  "+self.players_list['p3'].name
        elif self.nbj == 4:
            res += "  -  "+self.players_list['p4'].name

        return res

    def isAvailable(self, name):

        if self.players_list['p1'].name == name or self.players_list['p2'].name == name or self.players_list['p3'].name == name or self.players_list['p4'].name == name:
            return False

        return True

    def updateGameStatus(self):
        self.server.send_msg('V_gameStatus:'+self.master.labelGameStatusValue["text"]+' '+self.master.labelNbp["text"]+' - '+self.master.labelPlayersList["text"])

    def updatePlayersInfo(self):
        p1info = self.players_list['p1'].name +'/'+str(self.players_list['p1'].nbcards)
        p2info = self.players_list['p2'].name + '/' + str(self.players_list['p2'].nbcards)
        p3info = self.players_list['p3'].name + '/' + str(self.players_list['p3'].nbcards)
        p4info = self.players_list['p4'].name + '/' + str(self.players_list['p4'].nbcards)
        self.server.send_msg('V_playersInfo:'+p1info+';'+p2info+';'+p3info+';'+p4info)

    def removePlayer(self, name):


        for key in self.players_list:
            if self.players_list[key].name == name:
                self.connected_players_nb -= 1
                self.players_list[key].name = ""
                self.players_list[key].nbcards = 0
                self.availableSlot.append(key)
                self.availableSlot.sort()
                self.takenSlot.remove(key)
                self.takenSlot.sort()

        self.master.labelPlayersList["text"] = str(self.playersListToStr())

        if (self.playing):
            # BLINKING INFO
            self.master.labelNbp["fg"] = "red"
            self.master.labelGameStatusValue["text"] = "INTERROMPU"
            self.master.labelGameStatusValue["fg"] = "black"
            time.sleep(0.3)
            self.master.labelGameStatusValue["text"] = "INTERROMPU !!"
            self.master.labelGameStatusValue["fg"] = "red"
            time.sleep(0.3)
            self.master.labelGameStatusValue["text"] = "INTERROMPU"
            self.master.labelGameStatusValue["fg"] = "black"
            time.sleep(0.3)
            self.master.labelGameStatusValue["text"] = "INTERROMPU !!"
            self.master.labelGameStatusValue["fg"] = "red"
            time.sleep(0.3)

            self.master.labelNbp["text"] = str(self.connected_players_nb) + " sur " + str(self.nbj)

            # Notification console
            self.writeConsole('[%s]: Déconnexion du joueur %s.\n' % (self.name, name))
            self.writeConsole('[%s]: Match interrompu!\n' % (self.name))
            time.sleep(1)
            self.writeConsole('{%s]: Fermeture du serveur dans 3 secondes...\n' % (self.name))
            time.sleep(1)
            self.writeConsole('[%s]: 2...\n' % (self.name))
            time.sleep(1)
            self.writeConsole('[%s]: 1...\n' % (self.name))
            time.sleep(1)
            self.writeConsole('{%s]: Au revoir!\n' % (self.name))
            time.sleep(0.5)
            self.on_closing()
        else:
            self.writeConsole('[%s]: Déconnexion du joueur %s.\n' % (self.name, name))
            self.master.labelNbp["text"] = str(self.connected_players_nb) + " sur " + str(self.nbj)

            if self.connected_players_nb < self.nbj:
                self.master.labelNbp["fg"] = "red"
                self.master.labelGameStatusValue["text"] = "EN ATTENTE DE JOUEURS"
                self.master.labelGameStatusValue["fg"] = "red"

        self.updateGameStatus()
        self.updatePlayersInfo()

    def addPlayer(self, name):
        # Incrementation du nb de joueurs
        self.connected_players_nb += 1
        # Ajout du nom de joueur dans la liste

        self.players_list[self.availableSlot[0]].name = name
        self.takenSlot.append(self.availableSlot[0])
        self.takenSlot.sort()
        self.availableSlot.remove(self.availableSlot[0])
        self.availableSlot.sort()


        self.master.labelPlayersList["text"] = str(self.playersListToStr())


        self.master.labelNbp["text"] = str(self.connected_players_nb) + " sur " + str(self.nbj)

        # Notification console
        self.writeConsole(
            '[%s]: Connexion du joueur %s (%s sur %s).\n' % (self.name, name, self.connected_players_nb, self.nbj))

        # Modif labels
        if self.connected_players_nb == self.nbj:
            self.master.labelNbp["fg"] = "green"
            self.master.labelGameStatusValue["text"] = "PARTIE EN COURS"
            self.master.labelGameStatusValue["fg"] = "green"
            self.playing = True
            self.init_play()

        self.updateGameStatus()
        self.updatePlayersInfo()

    # Ecrire dans la console texte GUI
    def writeConsole(self, msg):
        self.master.console.insert(END, "\n[%s]#Serveur: %s" % (self.getTime(), msg))

    # Retourne l'heure exacte (utilisé pour la console GUI)
    def getTime(self):
        return datetime.strftime(datetime.now(), '%H:%M:%S')

    def on_closing(self):
        if self.isStarted:
            self.server.send_msg("V_disconnect")
        self.master.destroy()
        sys.exit(1)

    ##########################
    # Fonctions de JEU
    ##########################

    # Distribution initiale
    def players_decks_init(self, bdeck: basic_deck):

        #print(bdeck)

        print(bdeck)

        # On mélange le deck de base
        bdeck.shuffle()

        def chunks(l, n):
            n = max(1, n)
            return [l[i:i + n] for i in range(0, len(l), n)]

        size = len(bdeck)
        dsize2 = (size // 2)
        dsize3 = (size // 3)
        dsize4 = (size // 4)

        if self.nbj == 2:
            cardsForTwo = chunks(bdeck, dsize2)

            self.players_list['p1'].deck = basic_deck("Deck joueur 1", cardsForTwo[0])
            self.players_list['p2'].deck = basic_deck("Deck joueur 2", cardsForTwo[1])
            self.players_list['p1'].nbcards = dsize2
            self.players_list['p2'].nbcards = dsize2



            self.players_list['p1'].deck.shuffle()
            self.players_list['p2'].deck.shuffle()

        elif self.nbj == 3:
            cardsForThree = chunks(bdeck, dsize3)

            self.players_list['p1'].deck = basic_deck("Deck joueur 1", cardsForThree[0])
            self.players_list['p2'].deck = basic_deck("Deck joueur 2", cardsForThree[1])
            self.players_list['p3'].deck = basic_deck("Deck joueur 2", cardsForThree[2])
            self.players_list['p1'].nbcards = dsize3
            self.players_list['p2'].nbcards = dsize3
            self.players_list['p3'].nbcards = dsize3

            self.players_list['p1'].deck.shuffle()
            self.players_list['p2'].deck.shuffle()
            self.players_list['p3'].deck.shuffle()
        elif self.nbj == 4:
            cardsForFour = chunks(bdeck, dsize4)

            self.players_list['p1'].deck = basic_deck("Deck joueur 1", cardsForFour[0])
            self.players_list['p2'].deck = basic_deck("Deck joueur 2", cardsForFour[1])
            self.players_list['p3'].deck = basic_deck("Deck joueur 3", cardsForFour[2])
            self.players_list['p4'].deck = basic_deck("Deck joueur 4", cardsForFour[3])
            self.players_list['p1'].nbcards = dsize4
            self.players_list['p2'].nbcards = dsize4
            self.players_list['p3'].nbcards = dsize4
            self.players_list['p4'].nbcards = dsize4

            self.players_list['p1'].deck.shuffle()
            self.players_list['p2'].deck.shuffle()
            self.players_list['p3'].deck.shuffle()
            self.players_list['p4'].deck.shuffle()




    # Affrontement entre les joueurs
    def battle(self):

        print("FIGHT! \n")

        if self.battlefield == []:
            self.battlefield = self.takenSlot

        maxIndex = []
        maxValue = 0


        print(self.battlefield)

        for i in self.battlefield:
            if self.pot[i][-1].value > maxValue:
                maxIndex = [i]
                maxValue = self.pot[i][-1].value
            elif self.pot[i][-1].value == maxValue:
                maxIndex.append(i)



        if len(maxIndex) == 1: ## UN SEUL GAGNANT!
            winner = maxIndex[0]
            self.writeConsole("Le joueur "+self.players_list[winner].name+" remporte cette bataille!")


            ## AJOUT DES CARTES DU POT AU JOUEUR GAGNANT

            print(self.players_list[winner].deck)

            for p in self.takenSlot:
                self.players_list[winner].deck.extend(self.pot[p])
                print(self.pot[p])
                self.players_list[winner].nbcards = len(self.players_list[winner].deck)


            print(self.players_list[winner].deck)

            self.players_list[winner].deck.shuffle()

            self.updateViews()
            self.server.send_msg("V_battleWinner:"+winner+':'+self.players_list[winner].name)

            for p in self.takenSlot:
                if self.players_list[p].nbcards < 1:
                    print(i + " AUCUNE CARTE")
                    print(self.takenSlot)
                    if p in self.takenSlot:
                        self.takenSlot.remove(p)
                        #self.takenSlot.sort()
                    print(self.takenSlot)

        elif len(maxIndex) > 1: ## EGALITE ENTRE PLUSIEURS JOUEURS

            self.battlefield = maxIndex


            for i in maxIndex:
                if self.backCardOn:
                    ##BACKCARD
                    if len(self.players_list.get(i).deck) > 0:
                        self.pot[i].append(self.players_list.get(i).deck.first().clone())
                        self.players_list.get(i).deck.remove_first()
                        self.players_list.get(i).nbcards = len(self.players_list.get(i).deck)
                    else:
                        ## JOUEUR SANS CARTE
                        print(i +" AUCUNE CARTE")
                        print(self.takenSlot)
                        if i in self.takenSlot:
                            self.takenSlot.remove(i)
                            #self.takenSlot.sort()
                        if i in self.battlefield:
                            self.battlefield.remove(i)
                            #self.battlefield.sort()
                        print(self.takenSlot)

                ##NEWCARD
                if len(self.players_list.get(i).deck) > 0:
                    self.pot[i].append(self.players_list.get(i).deck.first().clone())
                    self.players_list.get(i).deck.remove_first()
                    self.players_list.get(i).nbcards = len(self.players_list.get(i).deck)
                else:
                    ## JOUEUR SANS CARTE
                    print(i + " AUCUNE CARTE")
                    print(self.takenSlot)
                    if i in self.takenSlot:
                        self.takenSlot.remove(i)
                        #self.takenSlot.sort()
                    if i in self.battlefield:
                        self.battlefield.remove(i)
                        #self.battlefield.sort()
                    print(self.takenSlot)

            self.battle()


    def init_play(self):
        #bdeck = french54_deck()
        bdeck = deck_test()
        # Initialisation des decks des joueurs avec le deck de base
        self.players_decks_init(bdeck)
        self.writeConsole("Début de la partie! Distribution des cartes...")

if __name__ == "__main__":
    root = Tk()
    root.wm_attributes("-topmost", 1)
    app = App(root)
    root.protocol("WM_DELETE_WINDOW", app.on_closing)
    root.mainloop()
