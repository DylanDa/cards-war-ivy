'''
Created on 13 avr. 2016

@author: dylan
'''

from model.card import card
from model.basic_deck import basic_deck


class deck_test(basic_deck):

    def __init__(self):

        self.name = "Jeu de cartes de test"

        red_symbols = ["Coeur", "Carreau"]
        black_symbols = ["Trèfle", "Pique"]
        ranks = ["2", "3"]

        for rs in red_symbols:
            for key,r in enumerate(ranks):
                self.append(card("Rouge",r,rs,key+1))


        for bs in black_symbols:
            for key, r in enumerate(ranks):
                self.append(card("Noir", r, bs, key+1))

        self.append(card("Noir", "Joker", "", len(ranks)+1))
        self.append(card("Rouge", "Joker", "", len(ranks)+1))