'''
Created on 13 avr. 2016

@author: dylan
'''

from copy import deepcopy

class card(object):

    def __init__(self, color, rank, symbol, value):
        self.color=color
        self.rank=rank
        self.symbol=symbol
        self.text=''+rank+' '+symbol+' '+color
        self.value=value
        self.visible=True
    def flip(self):
        self.visible=not(self.visible)

    def __repr__(self):
        return "<Carte %s>" % (self.text)

    def __str__(self):
        return self.text

    def clone(self):
        return deepcopy(self)

    def print(self):
        print(self.text)