'''
Created on 13 avr. 2016

@author: dylan
'''

from model.card import card
from model.basic_deck import basic_deck


class french54_deck(basic_deck):

    def __init__(self):

        self.name = "Jeu de 54 cartes"

        red_symbols = ["Coeur", "Carreau"]
        black_symbols = ["Trèfle", "Pique"]
        ranks = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "Valet", "Dame", "Roi", "As"]

        for rs in red_symbols:
            for key,r in enumerate(ranks):
                self.append(card("Rouge",r,rs,key+1))

        for bs in black_symbols:
            for key, r in enumerate(ranks):
                self.append(card("Noir", r, bs, key+1))

        self.append(card("Noir", "Joker", "", len(ranks)+1))
        self.append(card("Rouge", "Joker", "", len(ranks)+1))