'''
Created on 13 avr. 2016

@author: dylan
'''

from model.card import card
from random import *


class basic_deck(list):

    def __init__(self, name, seq=()):
        self.name = name
        super().__init__(seq)

    def __repr__(self):
        return "<%s size:%s>" % (self.name, len(self))

    def __str__(self):
        res = self.name+"\n"
        for c in self:
            res += c.text + "\n"
        return res

    def print(self):
        print(self)

    def shuffle(self):
        shuffle(self)
        shuffle(self)
        shuffle(self)

    def first(self)->card:
        if self:
            return self[0]
        else:
            # Défaite, carte sans valeur
            return card("null","null","null",0)

    def remove_first(self):
        if self:
            self.pop(0)
